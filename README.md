# Dockerized PHP Env

PHP on Docker (With composer and an apache running)

**Instructions**

To pull it:
`docker pull germanb/ubuphp:7.3`

To build it (From the Dockerfile's directory):
`docker build -t ubuphp:latest .`

To run it:
`docker run -dit --name ubuphp ubuphp:latest`

**Recommendations**

There's an apache server listening on port 80 by default on the 
default apache location, so maybe you'd want to map a volume to that 
location so you can edit the code with your favourite code editor.

To create the volume:
`docker volume create --driver local -o type=btrfs -o device=<your_preferred_local_directory> -o o=bind --name application`

And then, when you build it:
`docker run -dit -v application:/var/www/html --name ubuphp ubuphp:latest`

That's it! :D
